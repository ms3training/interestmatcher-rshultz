package com.rshultz.web.interestmatcher.model;

import java.util.Arrays;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

@Document(indexName = "interestmatcher", type = "user")
public class Account {
	@Id
	private String id;
	private String bio;
	@Size(min=5, max=16, message="Usernames range from 5 to 16 characters in length.")
	private String username;
	@Size(min=5, max=16, message="Passwords range from 6 to 32 characters in length.")
	private String password;
	@Email(message="Email addresses must be well-formed.")
	private String email;
	@Digits(fraction = 0, integer = 2, message = "Ages must range from 0 to 99 years.")
	private Short age;
	private String[] tags;

	public Account(String bio, String username, String password, String email, Short age, String[] tags) {
		this.bio = bio;
		this.username = username;
		this.password = password;
		this.email = email;
		this.age = age;
		this.tags = tags;
	}

	public Account() {

	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBio() {
		return bio;
	}

	public void setBio(String bio) {
		this.bio = bio;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Short getAge() {
		return age;
	}

	public void setAge(Short age) {
		this.age = age;
	}

	public String[] getTags() {
		return tags;
	}

	public void setTags(String[] tags) {
		this.tags = tags;
	}
	
	public String tagsToString() {
		String tagList = "";
		String[] tagArray = this.getTags();
		if (tagArray != null)
			for (int i = 0; i < tagArray.length; i++) {
				tagList += tagArray[i];
				if (i < tagArray.length - 1)
					tagList += ", ";
			}
		return tagList;
	}

	@Override
	public String toString() {
		return "Account [id=" + id + ", bio=" + bio + ", username=" + username + ", password=" + password + ", email="
				+ email + ", age=" + age + ", tags=" + Arrays.toString(tags) + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((age == null) ? 0 : age.hashCode());
		result = prime * result + ((bio == null) ? 0 : bio.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		result = prime * result + Arrays.hashCode(tags);
		result = prime * result + ((username == null) ? 0 : username.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Account other = (Account) obj;
		if (age == null) {
			if (other.age != null)
				return false;
		} else if (!age.equals(other.age))
			return false;
		if (bio == null) {
			if (other.bio != null)
				return false;
		} else if (!bio.equals(other.bio))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (!Arrays.equals(tags, other.tags))
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}

	
	
}
