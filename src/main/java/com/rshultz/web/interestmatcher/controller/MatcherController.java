package com.rshultz.web.interestmatcher.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.rshultz.web.interestmatcher.model.Account;
import com.rshultz.web.interestmatcher.repository.AccountRepository;

import java.io.IOException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.validation.Valid;

import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.data.elasticsearch.core.query.UpdateQuery;
import org.springframework.data.elasticsearch.core.query.UpdateQueryBuilder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import static org.elasticsearch.common.xcontent.XContentFactory.*;

@Controller
public class MatcherController {

	@Autowired
	AccountRepository accountRepository;

	@Autowired
	ElasticsearchTemplate elasticsearchTemplate;

	@RequestMapping("/")
	public String showHome() {
		return "home";
	}

	@RequestMapping("/login")
	public String showLogin() {
		return "login";
	}

	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String showRegister(Model model) {
		model.addAttribute("account", new Account());
		return "register";
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String doCreate(@Valid Account account, BindingResult result) {
		if (accountRepository.findAccountByUsernameIgnoreCase(account.getUsername()) != null)
			result.rejectValue("username", "DuplicateUsername.account.username",
					"A user with the name " + account.getUsername() + " already exists.");
		if (result.hasErrors()) {
			return "register";
		}
		accountRepository.save(account);
		return "accountcreated";
	}

	@RequestMapping(value = "/profile", method = RequestMethod.GET)
	public ModelAndView showProfile(Principal principal) {
		ModelAndView mav = new ModelAndView("profile");
		Account user = accountRepository.findAccountByUsernameIgnoreCase(principal.getName());
		mav.addObject("user", user);
		mav.addObject("account", new Account());
		if (user.getTags() != null) {
			String tagList = Arrays.toString(user.getTags());
			tagList = tagList.substring(1, tagList.length() - 1);
			mav.addObject("taglist", tagList);
		}
		return mav;
	}

	@RequestMapping(value = "/profile", method = RequestMethod.POST)
	public ModelAndView updateUserInfo(Account account, Principal principal) {
		ModelAndView mav = new ModelAndView("infoupdated");
		Account user = accountRepository.findAccountByUsernameIgnoreCase(principal.getName());
		mav.addObject("user", user);
		UpdateRequest updateRequest = new UpdateRequest();
		updateRequest.index("interestmatcher").type("user").id(user.getId());
		try {
			updateRequest.doc(jsonBuilder().startObject().field("age", account.getAge()).field("bio", account.getBio())
					.field("tags", account.getTags()).endObject());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		UpdateQuery updateQuery = new UpdateQueryBuilder().withId(user.getId()).withClass(Account.class)
				.withUpdateRequest(updateRequest).build();
		elasticsearchTemplate.update(updateQuery);
		return mav;
	}

	@RequestMapping(value = "/search", method = RequestMethod.GET)
	public ModelAndView viewUsersWithCommonInterests(Principal principal) {
		ModelAndView mav = new ModelAndView("search");
		Account user = accountRepository.findAccountByUsernameIgnoreCase(principal.getName());
		mav.addObject("user", user);
		QueryBuilder qb = QueryBuilders.matchQuery("tags", Arrays.toString(user.getTags()));
		SearchQuery searchQuery = new NativeSearchQueryBuilder().withQuery(qb).build();
		List<Account> users = elasticsearchTemplate.queryForList(searchQuery, Account.class);
		List<Account> modifiableUserList = new ArrayList<Account>(users);
		modifiableUserList.remove(user);
		mav.addObject("users", modifiableUserList);
		return mav;
	}

	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public ModelAndView viewResults(@RequestParam(required = false, name = "tagList") String[] tagArray,
			Principal principal) {
		ModelAndView mav = new ModelAndView("search");
		Account user = accountRepository.findAccountByUsernameIgnoreCase(principal.getName());
		mav.addObject("user", user);
		if (tagArray != null) {
			QueryBuilder qb = QueryBuilders.matchQuery("tags", Arrays.toString(tagArray));
			SearchQuery searchQuery = new NativeSearchQueryBuilder().withQuery(qb).build();
			List<Account> users = elasticsearchTemplate.queryForList(searchQuery, Account.class);
			List<Account> modifiableUserList = new ArrayList<Account>(users);
			modifiableUserList.remove(user);
			mav.addObject("users", modifiableUserList);
		}
		return mav;
	}
}
