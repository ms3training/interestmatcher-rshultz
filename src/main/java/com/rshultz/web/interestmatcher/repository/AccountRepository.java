package com.rshultz.web.interestmatcher.repository;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import com.rshultz.web.interestmatcher.model.Account;

public interface AccountRepository extends ElasticsearchRepository<Account, String> {

	public Account findAccountByUsernameIgnoreCase(String username);

}
